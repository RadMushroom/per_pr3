import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;


public class Main {
    static Robot r;
    static JFrame frame;
    static Cursor cursor;
    static int x = 0;
    static int y = 0;
    static boolean isChanged = false;
    static boolean isRobotMove = false;
    static boolean moveX = false;
    static boolean moveY = false;
    static int previousMouseX = 0;
    static int previousMouseY = 0;
    static int[] bounds = {31, 491, 9, 490};

    @SuppressWarnings("deprecation")
    public static void main(String[] args) throws AWTException {
        setFrame();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image image = toolkit.getImage("cursor.GIF");
        Point hotSpot = new Point(0, 0);
        cursor = toolkit.createCustomCursor(image, hotSpot, "cursor");
        frame.setCursor(cursor);
        r = new Robot();
        try {
            frame.addMouseMotionListener(new MouseMotionListener() {
                                             Robot r = new Robot();

                                             @Override
                                             public void mouseDragged(MouseEvent e) {
                                             }

                                             @Override
                                             public void mouseMoved(MouseEvent e) {
                                                 System.out.println("X: " + e.getX() + " Y:" + e.getY());
                                                 int dist_x = e.getX() - previousMouseX;
                                                 int dist_y = e.getY() - previousMouseY;
                                                 previousMouseX = e.getX();
                                                 previousMouseY = e.getY();
                                                 if (!isRobotMove) {
                                                     // check to intercept mouse movement that we caused
                                                     if (!moveX && !moveY) {
                                                         if (dist_x < 0)
                                                             r.mouseMove(e.getX() + Math.abs(dist_x), e.getY() + 2 * Math.abs(dist_x));
                                                         moveX = true;
                                                     } else if (!moveY) {
                                                         if (dist_y < 0)
                                                             r.mouseMove(e.getX() - 2 * Math.abs(dist_y), e.getY() + Math.abs(dist_y));
                                                         moveY = true;
                                                     } else {
                                                         if (dist_y > 0) r.mouseMove(e.getX(), e.getY() - 4 * dist_y);
                                                         moveX = false;
                                                         moveY = false;
                                                     }

                                                     // invert the mouse location
                                                     if (e.getY() == bounds[0]) {
                                                         r.mouseMove(e.getX(), bounds[1]);
                                                     } else if (e.getY() == bounds[1]) {
                                                         r.mouseMove(e.getX(), bounds[0]);
                                                     }

                                                     if (e.getX() == bounds[2]) {
                                                         r.mouseMove(bounds[3], e.getY());
                                                     } else if (e.getX() == bounds[3]) {
                                                         r.mouseMove(bounds[2], e.getY());
                                                     }
                                                 } else {
                                                     isRobotMove = false;
                                                     x = (int) MouseInfo.getPointerInfo().getLocation().getX();
                                                     y = (int) MouseInfo.getPointerInfo().getLocation().getY();
                                                 }
                                             }

                                         }

            );
            frame.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        if (!isChanged) {
                            frame.setCursor(Cursor.DEFAULT_CURSOR);
                            isChanged = true;
                        } else {
                            frame.setCursor(cursor);
                            isChanged = false;
                        }
                    } else {
                        System.exit(0);
                    }
                }

                @Override
                public void mousePressed(MouseEvent e) {
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                }

                @Override
                public void mouseExited(MouseEvent e) {
                }
            });

            frame.addMouseMotionListener(new MouseMotionAdapter() {
                @Override
                public void mouseMoved(MouseEvent e) {
                }
            });
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public static void setFrame() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setVisible(true);
    }
}
